package com.scf.notes.service.implementation;

import com.scf.notes.model.Notes;
import com.scf.notes.repository.NotesRepository;
import com.scf.notes.service.interf.NoteService;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NoteServiceImplementation extends AbstractCRUDService<Notes,Long> implements NoteService {
    private final NotesRepository notesRepository;
    @Override
    JpaRepository<Notes, Long> getRepository() {
        return notesRepository;
    }

}
