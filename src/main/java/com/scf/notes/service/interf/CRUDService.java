package com.scf.notes.service.interf;

import java.util.List;

public interface CRUDService<E,K> {
    void create(E entity);
    E getById(K id);
    List<E> getAll();

    E update(E entity);
    void delete(E entity);
}
