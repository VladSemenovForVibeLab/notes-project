package com.scf.notes.service.interf;

import com.scf.notes.model.Notes;

public interface NoteService extends CRUDService<Notes,Long>{
}
