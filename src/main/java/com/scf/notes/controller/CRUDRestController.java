package com.scf.notes.controller;

import com.scf.notes.service.interf.CRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class CRUDRestController<E,K> {
    abstract CRUDService<E,K> getService();

    @PostMapping("/create")
    public ResponseEntity<E> create(@RequestBody E object){
        getService().create(object);
        return new ResponseEntity<>(object, HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<E> getById(@PathVariable K id){
        E object = getService().getById(id);
        if(object==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(object,HttpStatus.OK);
    }
    @GetMapping("/all")
    public ResponseEntity<List<E>> findAll(){
        List<E> objects = getService().getAll();
        return new ResponseEntity<>(objects,HttpStatus.OK);
    }
    @PutMapping("/update")
    public ResponseEntity<E> update(@RequestBody E object){
        E updatedObject = getService().update(object);
        return new ResponseEntity<>(updatedObject,HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable K id){
        E objectForDelete = getService().getById(id);
        getService().delete(objectForDelete);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
