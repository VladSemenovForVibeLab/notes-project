package com.scf.notes.controller;

import com.scf.notes.model.Notes;
import com.scf.notes.service.interf.CRUDService;
import com.scf.notes.service.interf.NoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/notes")
@RequiredArgsConstructor
public class NoteController extends CRUDRestController<Notes,Long>{
    private final NoteService noteService;
    @Override
    CRUDService<Notes, Long> getService() {
        return noteService;
    }
}
