package com.scf.notes.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "notes")
@Schema(description = "NOTES")
public class Notes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Note id",example = "1")
    private Long id;
    @Column(name = "title")
    @Schema(description = "Note title",example = "First notes")
    private String title;
    @Column(name = "notes",columnDefinition = "LONGTEXT")
    @Schema(description = "notes",example = "This is my first notes in this application")
    private String notes;
}
