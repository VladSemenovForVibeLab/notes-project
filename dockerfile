FROM maven:3.9.4-amazoncorretto-21 AS build
WORKDIR /
COPY /src /src
COPY pom.xml /
RUN mvn -f /pom.xml clean package -DskipTests

FROM openjdk:21-jdk
WORKDIR /
COPY /src /src
COPY --from=build /target/*.jar notes-backend.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","notes-backend.jar"]